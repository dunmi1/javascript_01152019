
// const p = new Promise(resolve => {
//   setTimeout(() => {
//     resolve('a');
//   }, 2000);
// });


const p1 = new Promise(resolve => setTimeout(() => resolve('a'), 4000));
const p2 = new Promise(resolve => setTimeout(() => resolve('b'), 2000));

Promise.race([p1, p2]).then(result => {
  console.log(result);
});
