
const xhr = new XMLHttpRequest();

xhr.addEventListener('readystatechange', () => {

  console.log('ready state change was invoked');

  if (xhr.status < 400 && xhr.readyState === 4) {
    console.log(JSON.parse(xhr.responseText));
  }

});

// request without a request body
// xhr.open('GET', 'http://localhost:1250/cars');

// if you have no request body, pass nothing into the send
// xhr.send();

// request with a request body
xhr.open('POST', 'http://localhost:1250/cars');
xhr.setRequestHeader('Content-Type', 'application/json');

// if you have a request body, then pass it into the send
xhr.send(JSON.stringify({
  make: 'Chevrolet',
  model: 'Cruze',
  year: 2014,
}));